package form;

import java.util.Collection;
import java.util.stream.Collectors;

import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class UpgradeableCollection<T> extends ListModelList<T>{
	private static final long serialVersionUID = 3151910845223826315L;
	
	public ListModel<T> update(Collection<T> collection) {
		this.retainAll(collection);
		this.addAll(getNews(collection));
		return this;
	}
	
	public Collection<T> getNews(Collection<T> collection) {
		return collection
				.stream()
				.filter(cf -> !this.contains(cf))
				.collect(Collectors.toSet());
	}
}