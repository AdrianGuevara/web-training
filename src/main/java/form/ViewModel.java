package form;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModel;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ViewModel {
	
	private UpgradeableCollection<Form<Contact>> contactForms;
	private UpgradeableCollection<Form<Tag>> tagForms;
	private ContactForm contactForm;
	private TagForm selectedTagForm;
	private TagForm tagForm;
	@WireVariable
	private ContactRepository contactRepository;
	@WireVariable
	private TagRepository tagRepository;
	
	@Init
	public void init() {
		contactForms = new UpgradeableCollection<>();
		contactForms.setMultiple(true);
		tagForms = new UpgradeableCollection<>();
		contactForm = new ContactForm();
		selectedTagForm = new TagForm();
		tagForm = new TagForm();
	}

	@DependsOn({"contactForm.nameField", "contactForm.lastNameField", "contactForm.phoneField"})
	public ListModel<Form<Contact>> getContactForms() {
		return contactForms.update(ContactForm.from(contactRepository.findAll(contactForm)));
	}
	
	public TagForm getTagForm() {
		return tagForm;
	}

	public void setTagForm(TagForm tagForm) {
		this.tagForm = tagForm;
	}

	@DependsOn({"tagForm.nameField"})
	public ListModel<Form<Tag>> getTagForms() {
		return tagForms.update(TagForm.from(tagRepository.findAll()));
	}
	
	public ContactForm getContactForm() {
		return contactForm;
	}

	public TagForm getSelectedTagForm() {
		return selectedTagForm;
	}

	public void setSelectedTagForm(TagForm selectedTagForm) {
		this.selectedTagForm = selectedTagForm;
	}

	@Command
	@NotifyChange({"contactForm"})
	public void saveContactForm() {
		contactRepository.save(contactForm.create());
		clearContactForm();
	}
	
	@Command
	@NotifyChange({"contactForms"})
	public void updateContactForm(@BindingParam("contactForm") ContactForm contactForm) {
		contactRepository.save(contactForm.create());
		contactForm.setEditingStatus(false);
	}
	
	@Command
	@NotifyChange({"contactForms"})
	public void deleteContactForm(@BindingParam("contactForm") ContactForm contactForm) {
		contactRepository.delete(contactForm.create());
	}

	@Command
	@NotifyChange("contactForm")
	public void clearContactForm() {
		contactForm.clear();
	}
	
	@Command
	@NotifyChange({"tagForm"})
	public void saveTagForm() {
		tagRepository.save(tagForm.create());
		clearTagForm();
	}
	
	@Command
	@NotifyChange({"tagForms"})
	public void updateTagForm(@BindingParam("tagForm") TagForm tagForm) {
		tagRepository.save(tagForm.create());
		tagForm.setEditingStatus(false);
	}
	
	@Command
	@NotifyChange({"tagForms"})
	public void deleteTagForm(@BindingParam("tagForm") TagForm tagForm) {
		tagRepository.delete(tagForm.create());
	}
	
	@Command
	@NotifyChange("tagForm")
	public void clearTagForm() {
		tagForm.clear();
	}

	@Command
	public void switchContactFormEditingStatus(@BindingParam("form") ContactForm contactForm) {
		contactForm.switchEditingStatus();
		contactForms.notifyChange(contactForm);
	}
	
	@Command
	@NotifyChange("selectedTagForm")
	public void switchTagFormEditingStatus(@BindingParam("form") TagForm tagForm) {
		tagForm.switchEditingStatus();
		this.selectedTagForm = tagForm;
		tagForms.notifyChange(tagForm);
	}
}
