package form;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="Tag.findAll", query="SELECT t FROM Tag t")
public class Tag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;
	private String name;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name="ContactTag",
		joinColumns=@JoinColumn(name="tag"),
		inverseJoinColumns=@JoinColumn(name="contact"))
	private Set<Contact> contacts;
	
	public Tag(Tag other) {
		this.name = other.name;
		this.id = other.id;
		this.contacts = other.getContacts();
	}
	
	public Tag(String name, Collection<Contact> contacts) {
		this.name = name;
		this.setContacts(contacts);
	}

	public Tag(int id, String name, Collection<Contact> contacts) {
		this.id = id;
		this.name = name;
		this.setContacts(contacts);
	}

	protected Tag() {
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Contact> getContacts() {
		return this.contacts;
	}
	
	public void setContacts(Collection<Contact> contacts) {
		this.contacts = new LinkedHashSet<>(contacts);
	}

	public void addContact(Contact contact) {
		this.contacts.add(contact);
	}
	
	public void removeContact(Contact contact) {
		this.contacts.remove(contact);
	}


}