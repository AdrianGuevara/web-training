package form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TagForm extends Form<Tag> {
	private int id;
	private String nameField;
	private List<Form<Contact>> contactForms;
	
	public TagForm() {
		this.contactForms = new ArrayList<>();
		clear();
	}
	
	public TagForm(int id, String nameField, Collection<Form<Contact>> contactForms) {
		this.id = id;
		this.nameField = nameField;
		this.contactForms = new ArrayList<>(contactForms);
	}
	
	public static Form<Tag> from(Tag tag) {
		return new TagForm(tag.getId(), tag.getName(), ContactForm.from(tag.getContacts()));
	}
	
	public static List<Form<Tag>> from(Collection<Tag> tags) {
		return tags
				.stream()
				.map(TagForm::from)
				.collect(Collectors.toList());
	}
	
	public String getNameField() {
		return nameField;
	}

	public void setNameField(String nameField) {
		this.nameField = nameField;
	}

	public List<Form<Contact>> getContactForms() {
		return contactForms;
	}

	public void setContactForms(List<Form<Contact>> contactForms) {
		this.contactForms = contactForms;
	}

	@Override
	public Tag create() {
		return new Tag(id, nameField, contactForms
				.stream()
				.map(Form::create)
				.collect(Collectors.toList()));
	}
	
	@Override
	public void clear() {
		id = 0;
		nameField = "";
		contactForms.clear();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof TagForm))
			return false;
		TagForm other = (TagForm) obj;
		return this.id == other.id;
	}

	@Override
	public int hashCode() {
		return 318 + Integer.valueOf(id).hashCode();
	}
}
