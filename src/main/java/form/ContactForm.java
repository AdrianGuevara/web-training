package form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class ContactForm extends Form<Contact> implements Specification<Contact> {
	private int id;
	private String nameField = "";
	private String lastNameField = "";
	private String phoneField = "";
	
	public ContactForm() {
	}
	
	public ContactForm(int id, String nameField, String lastNameField, String phoneField) {
		this.id = id;
		this.nameField = nameField;
		this.lastNameField = lastNameField;
		this.phoneField = phoneField;
	}
	
	public static List<Form<Contact>> from(Collection<Contact> contacs) {
		return contacs
				.stream()
				.map(ContactForm::from)
				.collect(Collectors.toList());
	}
	
	public static Form<Contact> from(Contact contact) {
		return new ContactForm(contact.getId(), contact.getName(), contact.getLastName(), contact.getPhone());
	}

	public String getNameField() {
		return nameField;
	}

	public void setNameField(String nameField) {
		this.nameField = nameField;
	}

	public String getLastNameField() {
		return lastNameField;
	}

	public void setLastNameField(String lastNameField) {
		this.lastNameField = lastNameField;
	}

	public String getPhoneField() {
		return phoneField;
	}

	public void setPhoneField(String phoneField) {
		this.phoneField = phoneField;
	}

	@Override
	public Contact create() {
		return new Contact(id, nameField, lastNameField, phoneField);
	}

	@Override
	public void clear() {
		id = 0;
		nameField = "";
		lastNameField = "";
		phoneField = "";
	}

	@Override
	public Predicate toPredicate(Root<Contact> contactTable, CriteriaQuery<?> query, CriteriaBuilder builder) {
		List<Predicate> predicates = new ArrayList<>();
		if(!nameField.isEmpty())
			predicates.add(builder.like(builder.lower(contactTable.get("name")),'%'+nameField+'%'));
		if(!lastNameField.isEmpty())
			predicates.add(builder.like(builder.lower(contactTable.get("lastName")),'%'+lastNameField+'%'));
		if(!phoneField.isEmpty())
			predicates.add(builder.like(builder.lower(contactTable.get("phone")),'%'+phoneField+'%'));
		return builder.and(predicates.toArray(new Predicate[0]));
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof ContactForm))
			return false;
		ContactForm other = (ContactForm) obj;
		return this.id == other.id;
	}

	@Override
	public int hashCode() {
		return 574 + Integer.valueOf(id).hashCode();
	}
}