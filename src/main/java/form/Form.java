package form;

public abstract class Form<T> {
	private boolean editable = false;

	public boolean isEditable() {
		return editable;
	}

	public void setEditingStatus(boolean editingStatus) {
		this.editable = editingStatus;
	}
	
	public void switchEditingStatus() {
		this.editable = !this.editable;
	}

	public abstract T create();
	
	public abstract void clear();
}
