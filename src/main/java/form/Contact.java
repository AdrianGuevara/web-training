package form;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the contacts database table.
 * 
 */
@Entity
@NamedQuery(name="Contact.findAll", query="SELECT c FROM Contact c")
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String lastName;
	private String name;
	private String phone;
	
	@ManyToMany(mappedBy="contacts")
	private Set<Tag> tags;

	protected Contact() {
	}
	
	public Contact(Contact other) {
		this.name = other.name;
		this.lastName = other.lastName;
		this.id = other.id;
		this.phone = other.phone;
		this.tags = other.getTags();
	}
	
	public Contact(String name, String lastName, String phone) {
		this.name = name;
		this.lastName = lastName;
		this.phone = phone;
		this.tags = new HashSet<>();
	}
	
	public Contact(int id, String name, String lastName, String phone) {
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.phone = phone;
		this.tags = new HashSet<>();
	}
	
	public boolean getIsNew() {
		return id == 0;
	}

	public int getId() {
		return this.id;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getFullName() {
		return name + " " + lastName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Set<Tag> getTags() {
		return Collections.unmodifiableSet(this.tags);
	}
	
	public void setTags(Collection<Tag> tags) {
		this.tags.clear();
		this.tags.addAll(tags);
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof Contact))
			return false;
		Contact other = (Contact) obj;
		return this.id == other.id;
	}

	@Override
	public int hashCode() {
		return 298 + Integer.valueOf(id).hashCode();
	}
}