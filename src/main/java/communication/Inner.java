package communication;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class Inner {
	private Outer outer;
	
	@Init
    public void init(@ExecutionArgParam("outer") Outer outer) {
		this.outer = outer;
    }
	
	public String getValue() {
		return outer.getValue();
	}

	public void setValue(String value) {
		outer.setValue(value);
	}
	
	@Command
	@NotifyChange("value")
	public void notifyChange() {
		
	}
}
