package calculator;

import java.util.function.DoubleBinaryOperator;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.NotifyChange;

public class ViewModel {
	Double first; 
	Double second;
	DoubleBinaryOperator operator = (x,y) -> x+y;
	
	public Double getFirst() {
		return first;
	}

	public void setFirst(Double first) {
		this.first = first;
	}

	public Double getSecond() {
		return second;
	}

	public void setSecond(Double second) {
		this.second = second;
	}

	@Command
	@NotifyChange("result")
	public void sum() {
		this.operator = (x, y) -> x + y;
	}
	
	@Command
	@NotifyChange("result")
	public void subtract() {
		this.operator = (x, y) -> x - y;
	}

	@Command
	@NotifyChange("result")
	public void multiply() {
		this.operator = (x, y) -> x * y;
	}
	
	@Command
	@NotifyChange("result")
	public void divide() {
		this.operator = (x, y) -> x / y;
	}
	
	@DependsOn({"first", "second"})
	public Double getResult() {
		if(first == null || second == null)
			return null;
		return operator.applyAsDouble(first, second);
	}
}
