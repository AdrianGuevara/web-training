package array;

import java.util.*;
import java.util.stream.Collectors;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.NotifyChange; 

public class ViewModel {
	private int[] array;
	private int search;
	private Random random = new Random();
	
	public ViewModel () {
		setLength(5);
	}
	public int getSearch() {
		return search;
	}

	public void setSearch(int search) {
		this.search = search;
	}

	@DependsOn("list")
	public int getMax() {
		return Arrays
				.stream(array)
				.reduce(Integer.MIN_VALUE, Math::max);
	}
	
	@DependsOn("list")
	public int getMin() {
		return Arrays
				.stream(array)
				.reduce(Integer.MAX_VALUE, Math::min);
	}
	
	public int getLength() {
		return this.array.length;
	}
	
	public void setLength(int length) {
		array = new int[length];
		generate();
	}
	
	public int[] getArray() {
		return array;
	}
	
	@DependsOn("length")
	public List<Integer> getList() {
		return Arrays.stream(array).boxed().collect(Collectors.toList());
	}
	
	@DependsOn({"search", "list"})
	public int getPosition() {
		return getList().indexOf(search);
	}
	
	@Command
	@NotifyChange("list")
	public void generate() {
		for(int i=0; i < getLength(); i++)
			array[i] = random.nextInt(1000);
	}
}
